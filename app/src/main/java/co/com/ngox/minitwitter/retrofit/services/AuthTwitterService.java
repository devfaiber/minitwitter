package co.com.ngox.minitwitter.retrofit.services;

import java.util.List;

import co.com.ngox.minitwitter.retrofit.request.RequestCreateTweet;
import co.com.ngox.minitwitter.retrofit.request.RequestPutProfile;
import co.com.ngox.minitwitter.retrofit.response.ResponseProfile;
import co.com.ngox.minitwitter.retrofit.response.Tweet;
import co.com.ngox.minitwitter.retrofit.response.TweetDelete;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;


/**
 * SERVICIOS PRIVADOS QUE REQUIEREN EL TOKEN DE INICIO DE SESSION
 *
 *
 */

public interface AuthTwitterService {
    @GET("tweets/all")
    Call<List<Tweet>> getAllTweets();

    @POST("tweets/create")
    Call<Tweet> create(@Body RequestCreateTweet requestCreateTweet);

    @POST("tweets/like/{idTweet}")
    Call<Tweet> likeTweet(@Path("idTweet") int idTweet);

    @DELETE("tweets/{idTweet}")
    Call<TweetDelete> deleteTweet(@Path("idTweet") int idTweet);

    @GET("users/profile")
    Call<ResponseProfile> getProfile();

    @PUT("users/profile")
    Call<ResponseProfile> putProfile(@Body RequestPutProfile requestPutProfile);

}
