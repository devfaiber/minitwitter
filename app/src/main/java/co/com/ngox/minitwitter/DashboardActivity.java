package co.com.ngox.minitwitter;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

import co.com.ngox.minitwitter.common.Constantes;
import co.com.ngox.minitwitter.common.SharedPreferencesManager;
import co.com.ngox.minitwitter.ui.profile.ProfileFragment;
import co.com.ngox.minitwitter.ui.tweet.NuevoTweetDialogFragment;
import co.com.ngox.minitwitter.ui.tweet.TweetFragment;

public class DashboardActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    ImageView imageViewFoto;
    FloatingActionButton fab;
    BottomNavigationView navView;

    List<Fragment> fragmentList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        getSupportActionBar().hide();

        navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.

        fragmentList.add(TweetFragment.newInstance(Constantes.TWEET_LIST_ALL));
        fragmentList.add(TweetFragment.newInstance(Constantes.TWEET_LIST_FAV));
        fragmentList.add(new ProfileFragment());



        fab = findViewById(R.id.floatingActionButton);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NuevoTweetDialogFragment dialog = new NuevoTweetDialogFragment();
                dialog.show(getSupportFragmentManager(), "NuevoTweetDialogFragment");
            }
        });

        imageViewFoto = findViewById(R.id.imageViewFoto);
        String nameFoto = SharedPreferencesManager.getStringValue(Constantes.PREF_PHOTO);

        if(!nameFoto.isEmpty()){
            Glide.with(this)
                    .load(Constantes.UPLOAD_PHOTOS_API_URL+nameFoto)
                    .into(imageViewFoto);
        }


        navView.setOnNavigationItemSelectedListener(this);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentLayout, TweetFragment.newInstance(Constantes.TWEET_LIST_ALL))
                .commit();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fm = null;
        switch (item.getItemId()){
            case R.id.navigation_home:
                fab.show();
                fm = TweetFragment.newInstance(Constantes.TWEET_LIST_ALL);
                break;
            case R.id.navigation_tweets_like:
                fab.hide();
                fm = TweetFragment.newInstance(Constantes.TWEET_LIST_FAV);
                break;
            case R.id.navigation_profile:
                fab.hide();
                fm = fragmentList.get(2);
                break;
        }

        if(fm != null){
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentLayout, fm)
                    .commit();
        }

        return true;
    }
}