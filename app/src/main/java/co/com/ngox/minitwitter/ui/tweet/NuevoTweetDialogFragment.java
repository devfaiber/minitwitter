package co.com.ngox.minitwitter.ui.tweet;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;

import co.com.ngox.minitwitter.R;
import co.com.ngox.minitwitter.data.TweetViewModel;
import co.com.ngox.minitwitter.common.Constantes;
import co.com.ngox.minitwitter.common.SharedPreferencesManager;

public class NuevoTweetDialogFragment extends DialogFragment implements View.OnClickListener {
    Button btnTwittear;
    ImageView imgFoto, imgClose;
    EditText editTextMensaje;
    View view;
    TweetViewModel tweetViewModel;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // poner estilos
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.fragment_nuevo_tweet, container, false);

        findViews();
        configViews();
        events();
        return view;

    }
    private void findViews() {
        btnTwittear = view.findViewById(R.id.buttonTwittear);
        imgClose = view.findViewById(R.id.imageViewClose);
        imgFoto = view.findViewById(R.id.imageViewFoto);
        editTextMensaje = view.findViewById(R.id.editTextMensaje);
    }
    private void configViews(){
        String nameFoto = SharedPreferencesManager.getStringValue(Constantes.PREF_PHOTO);

        if(!nameFoto.isEmpty()){
            Glide.with(view)
                    .load(Constantes.UPLOAD_PHOTOS_API_URL+nameFoto)
                    .into(imgFoto);
        }
    }
    private void events(){
        btnTwittear.setOnClickListener(this);
        imgClose.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        String mensaje = editTextMensaje.getText().toString();

        if(id == R.id.imageViewClose){
            if(!mensaje.isEmpty()){
                showDialogConfirm();
            }else{
                getDialog().dismiss();
            }
        }else if(id == R.id.buttonTwittear){

            if(mensaje.equals("")){
                editTextMensaje.setError("Campo requerido para twittear");
            }else{
                tweetViewModel = new ViewModelProvider(getActivity()).get(TweetViewModel.class);
                tweetViewModel.insetTweet(mensaje);
                getDialog().dismiss();
            }

        }
    }

    private void showDialogConfirm() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle("Cancelar Tweet")
                .setMessage("¿Deseas realmente cancelar el tweet?. el mensaje se eliminara.")
                .setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        getDialog().dismiss();
                    }
                })
                .setNegativeButton("cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog alerta = builder.create();
        alerta.show();

    }
}
