package co.com.ngox.minitwitter.retrofit;

import co.com.ngox.minitwitter.common.Constantes;
import co.com.ngox.minitwitter.retrofit.services.MiniTwitterService;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MiniTwitterClient {
    private static MiniTwitterClient INSTANCE = null;
    private MiniTwitterService miniTwitterService;
    private Retrofit retrofit;
    public MiniTwitterClient(){
        retrofit = new Retrofit.Builder()
                .baseUrl(Constantes.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        miniTwitterService = retrofit.create(MiniTwitterService.class);
    }
    public static MiniTwitterClient getInstance(){
        if(INSTANCE == null){
            INSTANCE = new MiniTwitterClient();
        }
        return INSTANCE;

    }

    public MiniTwitterService getMiniTwitterService(){
        return miniTwitterService;
    }
}
