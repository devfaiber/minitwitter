package co.com.ngox.minitwitter.ui.profile;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import co.com.ngox.minitwitter.common.Constantes;
import co.com.ngox.minitwitter.data.ProfileViewModel;
import co.com.ngox.minitwitter.R;
import co.com.ngox.minitwitter.retrofit.request.RequestPutProfile;
import co.com.ngox.minitwitter.retrofit.response.ResponseProfile;

public class ProfileFragment extends Fragment {

    private ProfileViewModel profileViewModel;
    private View view;
    private ImageView imageViewFoto;
    private EditText editTextUsername, editTextEmail, editTextPassword, editTextWebsite, editTextDescripcion;
    private Button btnGuardar, btnCambioPassword;
    private boolean loadingData;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        profileViewModel = new ViewModelProvider(this).get(ProfileViewModel.class);
        loadingData = true;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        findViews();
        configViews();
        return view;
    }

    private void configViews() {
        profileViewModel.getProfile().observe(getActivity(), new Observer<ResponseProfile>(){

            @Override
            public void onChanged(ResponseProfile responseProfile) {
                loadingData = false;

                editTextUsername.setText(responseProfile.getUsername());
                editTextEmail.setText(responseProfile.getEmail());
                editTextWebsite.setText(responseProfile.getWebsite());
                editTextDescripcion.setText(responseProfile.getDescripcion());

                if(!loadingData){
                    btnGuardar.setEnabled(true);
                    Toast.makeText(getActivity(), "Datos guardados exitosamente", Toast.LENGTH_SHORT).show();
                }

                if(responseProfile.getPhotoUrl().equals("")){
                    imageViewFoto.setImageResource(R.mipmap.ic_launcher_round);
                }else{
                    Glide.with(getActivity())
                            .load(Constantes.UPLOAD_PHOTOS_API_URL + responseProfile.getPhotoUrl())
                            .into(imageViewFoto);
                }


            }
        });
    }

    private void findViews() {
        imageViewFoto = view.findViewById(R.id.imageViewFoto);
        editTextUsername = view.findViewById(R.id.editTextUsername);
        editTextEmail = view.findViewById(R.id.editTextEmail);
        editTextPassword = view.findViewById(R.id.editTextPassword);
        editTextWebsite = view.findViewById(R.id.editTextWebsite);
        editTextDescripcion = view.findViewById(R.id.editTextDescripcion);

        btnGuardar = view.findViewById(R.id.buttonGuardar);
        btnCambioPassword = view.findViewById(R.id.buttonCambioPassword);

        btnGuardar.setOnClickListener(view1 -> {
            toUpdateProfile();
        });


    }

    private void toUpdateProfile() {
        String username = editTextUsername.getText().toString();
        String email = editTextEmail.getText().toString();
        String website = editTextWebsite.getText().toString();
        String descripcion = editTextDescripcion.getText().toString();
        String password = editTextPassword.getText().toString();

        RequestPutProfile requestPutProfile = new RequestPutProfile(username, email, descripcion, website, password);

        if(validation(username, email, password)){
            btnGuardar.setEnabled(false);
            profileViewModel.putProfile(requestPutProfile);
        }
    }

    private boolean validation(String username, String email, String password) {
        boolean estado = true;

        if(username.isEmpty()){
            editTextUsername.setError("Este campo es requerido");
            estado = false;
        }else if(email.isEmpty()){
            editTextEmail.setError("Este campo es requerido");
            estado = false;
        }else if(password.isEmpty()){
            editTextPassword.setError("Este campo es obligatorio para validar que eres tu");
            estado = false;
        }

        return estado;
    }


}