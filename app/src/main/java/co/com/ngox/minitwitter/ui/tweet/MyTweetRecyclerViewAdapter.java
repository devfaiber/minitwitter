package co.com.ngox.minitwitter.ui.tweet;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;

import java.util.List;

import co.com.ngox.minitwitter.R;
import co.com.ngox.minitwitter.data.TweetViewModel;
import co.com.ngox.minitwitter.common.Constantes;
import co.com.ngox.minitwitter.common.SharedPreferencesManager;
import co.com.ngox.minitwitter.retrofit.response.Like;
import co.com.ngox.minitwitter.retrofit.response.Tweet;


public class MyTweetRecyclerViewAdapter extends RecyclerView.Adapter<MyTweetRecyclerViewAdapter.ViewHolder> {

    private List<Tweet> mValues;
    private Context context;
    private String usernameLogueado;
    private TweetViewModel tweetViewModel;

    public MyTweetRecyclerViewAdapter(Context p_context, List<Tweet> items) {
        mValues = items;
        context = p_context;
        usernameLogueado = SharedPreferencesManager.getStringValue(Constantes.PREF_USUARIO);
        tweetViewModel = new ViewModelProvider((FragmentActivity)context).get(TweetViewModel.class);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_tweet, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // si es nulo detiene todx el proceso de abajo
        if(mValues == null) return;

        holder.mItem = mValues.get(position);
        holder.textViewUsername.setText("@"+holder.mItem.getUser().getUsername());
        holder.textViewMessage.setText(holder.mItem.getMensaje());
        holder.textViewNumLikes.setText(String.valueOf(holder.mItem.getLikes().size()));

        String photo = holder.mItem.getUser().getPhotoUrl();

        if(!photo.equals("")){
            Glide.with(context)
                    .load(Constantes.UPLOAD_PHOTOS_API_URL + photo)
                    .into(holder.imageViewFoto);
        }else{
            Glide.with(context)
                    .load(R.mipmap.ic_launcher_round)
                    .into(holder.imageViewFoto);
        }

        // resetear los estilos de like y texto
        holder.imageViewFavorito.setImageResource(R.drawable.ic_baseline_favorite_border_24);
        holder.textViewNumLikes.setTextColor(context.getResources().getColor(android.R.color.black));
        holder.textViewNumLikes.setTypeface(null, Typeface.NORMAL);


        holder.imageViewFavorito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tweetViewModel.likeTweet(holder.mItem.getId());
            }
        });

        for (Like like: holder.mItem.getLikes()){
            if(like.getUsername().equals(usernameLogueado)){
                holder.imageViewFavorito.setImageResource(R.drawable.ic_baseline_favorite_24_black);
                holder.textViewNumLikes.setTextColor(context.getResources().getColor(R.color.colorPink));
                holder.textViewNumLikes.setTypeface(null, Typeface.BOLD);
                break;
            }
        }

        if(holder.mItem.getUser().getUsername().equals(usernameLogueado)){
            holder.imageViewOptiones.setVisibility(View.VISIBLE);
        }else{
            holder.imageViewOptiones.setVisibility(View.GONE); // desaparece por completo
        }
        holder.imageViewOptiones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tweetViewModel.openDialogTweet(context, holder.mItem.getId());
            }
        });

    }

    @Override
    public int getItemCount() {
        // para que no se caiga
        if(mValues != null){
            return mValues.size();
        }else{
            return 0;
        }
    }


    public void setData(List<Tweet> tweets){
        mValues = tweets;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView textViewUsername, textViewMessage, textViewNumLikes;
        public final ImageView imageViewFoto, imageViewFavorito, imageViewOptiones;
        public Tweet mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            imageViewFoto = mView.findViewById(R.id.imageViewFoto);
            imageViewFavorito = mView.findViewById(R.id.imageViewFavorito);
            textViewUsername = mView.findViewById(R.id.textViewUsername);
            textViewMessage = mView.findViewById(R.id.textViewMessage);
            textViewNumLikes = mView.findViewById(R.id.textViewNumLikes);
            imageViewOptiones = mView.findViewById(R.id.imageViewOptions);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + textViewUsername.getText() + "'";
        }
    }
}