package co.com.ngox.minitwitter.common;

import android.app.Application;
import android.content.Context;

public class MyApplication extends Application {
    private static MyApplication INSTANCE;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
    }

    public static MyApplication getInstance(){
        return INSTANCE;

    }

    public static Context getContext(){
        return INSTANCE.getBaseContext();
    }
}
