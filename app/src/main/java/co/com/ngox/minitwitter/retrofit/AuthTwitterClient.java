package co.com.ngox.minitwitter.retrofit;

import co.com.ngox.minitwitter.common.Constantes;
import co.com.ngox.minitwitter.retrofit.services.AuthTwitterService;
import co.com.ngox.minitwitter.retrofit.services.MiniTwitterService;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
/*
CONEXION RETROFIT PARA PETICION QUE REQUIEREN EL ENCABEZADO QUE ES EL TOKEN

 */

public class AuthTwitterClient {
    private static AuthTwitterClient INSTANCE = null;
    private AuthTwitterService authTwitterService;
    private Retrofit retrofit;
    public AuthTwitterClient(){

        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        okHttpClientBuilder.addInterceptor(new AuthInterceptor());
        OkHttpClient cliente = okHttpClientBuilder.build();


        retrofit = new Retrofit.Builder()
                .baseUrl(Constantes.BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(cliente)
                .build();

        authTwitterService = retrofit.create(AuthTwitterService.class);
    }
    public static AuthTwitterClient getInstance(){
        if(INSTANCE == null){
            INSTANCE = new AuthTwitterClient();
        }
        return INSTANCE;

    }

    public AuthTwitterService getAuthTwitterService(){
        return authTwitterService;
    }
}
