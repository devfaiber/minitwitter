package co.com.ngox.minitwitter.ui.auth;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import co.com.ngox.minitwitter.R;
import co.com.ngox.minitwitter.common.Constantes;
import co.com.ngox.minitwitter.common.SharedPreferencesManager;
import co.com.ngox.minitwitter.retrofit.MiniTwitterClient;
import co.com.ngox.minitwitter.retrofit.request.RequestRegister;
import co.com.ngox.minitwitter.retrofit.response.ResponseAuth;
import co.com.ngox.minitwitter.retrofit.services.MiniTwitterService;
import co.com.ngox.minitwitter.ui.auth.LoginFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegistrerFragment extends Fragment implements View.OnClickListener {
    Button btnRegister;
    TextView textViewLogin;
    View viewLayout;
    EditText editTextUsuario, editTextEmail, editTextPassword, editTextCofirmPassword;

    MiniTwitterClient miniTwitterClient;
    MiniTwitterService miniTwitterService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewLayout = inflater.inflate(R.layout.fragment_registrer, container, false);

        retrofitInit();
        findViews();
        events();

        return viewLayout;
    }

    private void findViews(){
        btnRegister = viewLayout.findViewById(R.id.buttonRegister);
        textViewLogin = viewLayout.findViewById(R.id.textViewLogin);

        editTextUsuario = viewLayout.findViewById(R.id.editTextUsername);
        editTextEmail = viewLayout.findViewById(R.id.editTextEmail);
        editTextPassword = viewLayout.findViewById(R.id.editTextPassword);
        editTextCofirmPassword = viewLayout.findViewById(R.id.editTextVerifyPassword);

    }
    private void events(){
        btnRegister.setOnClickListener(this);
        textViewLogin.setOnClickListener(this);
    }
    private void retrofitInit(){
        miniTwitterClient = MiniTwitterClient.getInstance();
        miniTwitterService = miniTwitterClient.getMiniTwitterService();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.textViewLogin:
                toLogin();
                break;
            case R.id.buttonRegister:

                toRegister();
                break;
        }
    }

    private void toRegister() {
        String usuario = editTextUsuario.getText().toString();
        String email = editTextEmail.getText().toString();
        String password = editTextPassword.getText().toString();

        if(validation()){
            btnRegister.setEnabled(false);
            Call<ResponseAuth> call = miniTwitterService.doRegister(new RequestRegister(
                    usuario, email, password, Constantes.CODE_API
            ));


            call.enqueue(new Callback<ResponseAuth>() {
                @Override
                public void onResponse(Call<ResponseAuth> call, Response<ResponseAuth> response) {
                    btnRegister.setEnabled(true);
                    if(response.isSuccessful()){

                        ResponseAuth respuesta = response.body();

                        SharedPreferencesManager.setStringValue(Constantes.PREF_TOKEN, respuesta.getToken());
                        SharedPreferencesManager.setStringValue(Constantes.PREF_USUARIO, respuesta.getUsername());
                        SharedPreferencesManager.setStringValue(Constantes.PREF_EMAIL, respuesta.getEmail());
                        SharedPreferencesManager.setStringValue(Constantes.PREF_PHOTO, respuesta.getPhotoUrl());
                        SharedPreferencesManager.setStringValue(Constantes.PREF_FEC_CREATE, respuesta.getCreated());

                        Toast.makeText(getContext(), "Registro exitoso "+respuesta.getUsername(), Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(getContext(), "Verifique el usuario o email", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseAuth> call, Throwable t) {
                    btnRegister.setEnabled(false);
                    Toast.makeText(getContext(),"NO hay conexion a internet, intente mas tarde", Toast.LENGTH_LONG).show();
                }
            });


        }
    }

    private boolean validation(){
        boolean estado = true;

        if(editTextUsuario.getText().toString().equals("")){
            editTextUsuario.setError("Campo requerido");
            estado = false;
        }else if (editTextEmail.getText().toString().equals("")){
            editTextEmail.setError("Campo requerido");
            estado = false;
        }else if(editTextPassword.getText().toString().equals("")){
            editTextPassword.setError("Campo requerido");
            estado = false;
        }else if(!editTextCofirmPassword.getText().toString().equals(editTextPassword.getText().toString())){
            editTextCofirmPassword.setError("Esta contraseña no coincide con la ingresada");
            estado = false;
        }

        return estado;
    }

    private void toLogin() {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentLayout, new LoginFragment())
                .commit();
    }
    private void clearFields(){
        editTextUsuario.setText("");
        editTextPassword.setText("");
        editTextCofirmPassword.setText("");
        editTextEmail.setText("");
    }
}