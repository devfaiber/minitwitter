package co.com.ngox.minitwitter.common;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesManager{


    public static SharedPreferences getInstance(){

        return MyApplication.getContext().getSharedPreferences(Constantes.SHARED_PREFERENCES_1,
                Context.MODE_PRIVATE);
    }

    public static void setStringValue(String key, String value){
        SharedPreferences.Editor editor = getInstance().edit();
        editor.putString(key, value);
        editor.apply();
    }
    public static void setBooleanValue(String key, boolean value){
        SharedPreferences.Editor editor = getInstance().edit();
        editor.putBoolean(key, value);
        editor.apply();

    }

    public static String getStringValue(String key){
        return getInstance().getString(key, "");
    }
    public static boolean getBooleanValue(String key){
        return getInstance().getBoolean(key, false);
    }

}



