package co.com.ngox.minitwitter.data;

import android.content.Context;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;

import co.com.ngox.minitwitter.common.MyApplication;
import co.com.ngox.minitwitter.retrofit.AuthTwitterClient;
import co.com.ngox.minitwitter.retrofit.request.RequestPutProfile;
import co.com.ngox.minitwitter.retrofit.response.ResponseProfile;
import co.com.ngox.minitwitter.retrofit.services.AuthTwitterService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileRepository {
    private Context contexto;
    private AuthTwitterClient authTwitterClient;
    private AuthTwitterService authTwitterService;

    private MutableLiveData<ResponseProfile> userProfile;


    public ProfileRepository(Context context){
        contexto = context;
        authTwitterClient = AuthTwitterClient.getInstance();
        authTwitterService = authTwitterClient.getAuthTwitterService();
        userProfile = getUserProfile();


    }
    public MutableLiveData<ResponseProfile> getUserProfile(){

        if(userProfile == null){
            userProfile = new MutableLiveData<>();
        }

        Call<ResponseProfile> call = authTwitterService.getProfile();
        call.enqueue(new Callback<ResponseProfile>() {
            @Override
            public void onResponse(Call<ResponseProfile> call, Response<ResponseProfile> response) {
                if (response.isSuccessful()){
                    userProfile.setValue(response.body());

                } else {
                    Toast.makeText(MyApplication.getContext(), "No hay datos por mostrar profile",
                            Toast.LENGTH_LONG)
                            .show();

                }
            }

            @Override
            public void onFailure(Call<ResponseProfile> call, Throwable t) {
                Toast.makeText(MyApplication.getContext(), "Hubo un error grave con los datos profile",
                        Toast.LENGTH_LONG)
                        .show();
            }
        });

        return userProfile;
    }

    public void putUserProfile(RequestPutProfile requestPutProfile){


        Call<ResponseProfile> call = authTwitterService.putProfile(requestPutProfile);
        call.enqueue(new Callback<ResponseProfile>() {
            @Override
            public void onResponse(Call<ResponseProfile> call, Response<ResponseProfile> response) {
                if(response.isSuccessful()){
                    userProfile.setValue(response.body());
                }else{
                    Toast.makeText(MyApplication.getContext(), "Error al actualizar", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseProfile> call, Throwable t) {
                Toast.makeText(MyApplication.getContext(), "Error de conexion", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
