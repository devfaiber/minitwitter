package co.com.ngox.minitwitter.data;

import android.content.Context;
import android.widget.Toast;


import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import co.com.ngox.minitwitter.common.Constantes;
import co.com.ngox.minitwitter.common.MyApplication;
import co.com.ngox.minitwitter.common.SharedPreferencesManager;
import co.com.ngox.minitwitter.retrofit.AuthTwitterClient;
import co.com.ngox.minitwitter.retrofit.request.RequestCreateTweet;
import co.com.ngox.minitwitter.retrofit.response.Like;
import co.com.ngox.minitwitter.retrofit.response.Tweet;
import co.com.ngox.minitwitter.retrofit.response.TweetDelete;
import co.com.ngox.minitwitter.retrofit.services.AuthTwitterService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TweetRepository {
    private Context contexto;
    private AuthTwitterClient authTwitterClient;
    private AuthTwitterService authTwitterService;

    private MutableLiveData<List<Tweet>> allTweets;
    private MutableLiveData<List<Tweet>> allFavTweets;
    private String usernameLogueado;

    public TweetRepository(Context context){
        contexto = context;
        authTwitterClient = AuthTwitterClient.getInstance();
        authTwitterService = authTwitterClient.getAuthTwitterService();
        allTweets = getAllTweets();
        usernameLogueado = SharedPreferencesManager.getStringValue(Constantes.PREF_USUARIO);

    }
    public MutableLiveData<List<Tweet>> getAllTweets(){

        if(allTweets == null){
            allTweets = new MutableLiveData<>();
        }

        Call<List<Tweet>> call = authTwitterService.getAllTweets();
        call.enqueue(new Callback<List<Tweet>>() {
            @Override
            public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
                if (response.isSuccessful()){
                    allTweets.setValue(response.body());

                } else {
                    Toast.makeText(MyApplication.getContext(), "No hay datos por mostrar",
                            Toast.LENGTH_LONG)
                            .show();

                }
            }

            @Override
            public void onFailure(Call<List<Tweet>> call, Throwable t) {
                Toast.makeText(MyApplication.getContext(), "Hubo un error grave con los datos",
                        Toast.LENGTH_LONG)
                        .show();
            }
        });

        return allTweets;
    }


    public void likeTweet(int idTweet){

        Call<Tweet> call = authTwitterService.likeTweet(idTweet);
        call.enqueue(new Callback<Tweet>() {
            @Override
            public void onResponse(Call<Tweet> call, Response<Tweet> response) {
                if(response.isSuccessful()){
                    List<Tweet> listaClon = new ArrayList<>();

                    for (int i = 0; i<allTweets.getValue().size(); i++){
                        if(allTweets.getValue().get(i).getId() == idTweet){
                            listaClon.add(response.body());
                        }else{
                            listaClon.add(new Tweet(allTweets.getValue().get(i)));
                        }
                    }

                    allTweets.setValue(listaClon);
                    getAllFavTweets();

                }else{
                    Toast.makeText(MyApplication.getContext(), "No se pudo crear el tweet",
                            Toast.LENGTH_LONG)
                            .show();
                }
            }

            @Override
            public void onFailure(Call<Tweet> call, Throwable t) {
                Toast.makeText(MyApplication.getContext(), "Hubo un error grave con los datos",
                        Toast.LENGTH_LONG)
                        .show();
            }
        });
    }

    public void createTweet(String mensaje){
        RequestCreateTweet requestCreateTweet = new RequestCreateTweet(mensaje);
        Call<Tweet> call = authTwitterService.create(requestCreateTweet);
        call.enqueue(new Callback<Tweet>() {
            @Override
            public void onResponse(Call<Tweet> call, Response<Tweet> response) {
                if(response.isSuccessful()){
                    List<Tweet> listaClon = new ArrayList<>();
                    // añade de primero el elemento

                    listaClon.add(response.body());
                    //listaClon.addAll(allTweets.getValue());

                    for (int i = 0; i<allTweets.getValue().size(); i++){
                        listaClon.add(new Tweet(allTweets.getValue().get(i)));
                    }

                    allTweets.setValue(listaClon);


                }else{
                    Toast.makeText(MyApplication.getContext(), "No se pudo crear el tweet",
                            Toast.LENGTH_LONG)
                            .show();
                }
            }

            @Override
            public void onFailure(Call<Tweet> call, Throwable t) {
                Toast.makeText(MyApplication.getContext(), "Hubo un error grave con los datos",
                        Toast.LENGTH_LONG)
                        .show();
            }
        });
    }

    public MutableLiveData<List<Tweet>> getAllFavTweets(){
        if(allFavTweets == null){
            allFavTweets = new MutableLiveData<>();
        }
        List<Tweet> newFavList = new ArrayList<>();

        Iterator iteratorTweet = allTweets.getValue().iterator();

        while(iteratorTweet.hasNext()){
            Tweet current = (Tweet) iteratorTweet.next();
            Iterator iteratorLike = current.getLikes().iterator();
            boolean encontrado = false;
            while (iteratorLike.hasNext()){
                Like like = (Like) iteratorLike.next();
                if(like.getUsername().equals(usernameLogueado)){
                    encontrado = true;
                    newFavList.add(current);
                    break;
                }
            }
        }

        allFavTweets.setValue(newFavList);

        return allFavTweets;
    }

    public void deleteTweet(int idTweet){
        Call<TweetDelete> call  = authTwitterService.deleteTweet(idTweet);
        call.enqueue(new Callback<TweetDelete>() {
            @Override
            public void onResponse(Call<TweetDelete> call, Response<TweetDelete> response) {
                if(response.isSuccessful()){
                    List<Tweet> listClon = new ArrayList<>();

                    for (int i = 0; i < allTweets.getValue().size(); i++) {
                        if(allTweets.getValue().get(i).getId() != idTweet){
                            listClon.add(new Tweet(allTweets.getValue().get(i)));
                        }
                    }

                    allTweets.setValue(listClon);
                    getAllFavTweets();
                }else{

                }
            }

            @Override
            public void onFailure(Call<TweetDelete> call, Throwable t) {
                Toast.makeText(MyApplication.getContext(), "Hubo un error grave con conexion",
                        Toast.LENGTH_LONG)
                        .show();
            }
        });

    }
}
