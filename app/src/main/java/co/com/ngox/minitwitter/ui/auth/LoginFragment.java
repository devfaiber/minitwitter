package co.com.ngox.minitwitter.ui.auth;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import co.com.ngox.minitwitter.DashboardActivity;
import co.com.ngox.minitwitter.R;
import co.com.ngox.minitwitter.common.Constantes;
import co.com.ngox.minitwitter.common.SharedPreferencesManager;
import co.com.ngox.minitwitter.retrofit.MiniTwitterClient;
import co.com.ngox.minitwitter.retrofit.request.RequestLogin;
import co.com.ngox.minitwitter.retrofit.response.ResponseAuth;
import co.com.ngox.minitwitter.retrofit.services.MiniTwitterService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginFragment extends Fragment implements View.OnClickListener {
    Button btnLogin;
    TextView textViewRegister;
    View viewLayout;
    EditText editTextEmail, editTextPassword;
    CheckBox checkBoxRecordar;
    ProgressBar progressBarLogin;
    //---
    MiniTwitterClient miniTwitterClient;
    MiniTwitterService miniTwitterService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewLayout = inflater.inflate(R.layout.fragment_login, container, false);
        retrofitInit();
        findViews();
        viewConfig();
        events();

        return viewLayout;
    }

    private void viewConfig() {
        String email = SharedPreferencesManager.getStringValue(Constantes.PREF_EMAIL);
        boolean is_recordar = SharedPreferencesManager.getBooleanValue(Constantes.PREF_IS_RECORDAR);

        if(is_recordar){
            editTextEmail.setText(email);
            checkBoxRecordar.setChecked(is_recordar);
        }

        /*if(!SharedPreferencesManager.getStringValue(Constantes.PREF_TOKEN).equals("")){
            Intent intent = new Intent(getActivity(), DashboardActivity.class);
            getActivity().startActivity(intent);
            getActivity().finish();
        }*/

    }

    private void findViews() {
        btnLogin = viewLayout.findViewById(R.id.buttonLogin);
        textViewRegister = viewLayout.findViewById(R.id.textViewRegister);
        progressBarLogin = viewLayout.findViewById(R.id.progressBarLogin);

        editTextEmail = viewLayout.findViewById(R.id.editTextEmail);
        editTextPassword = viewLayout.findViewById(R.id.editTextPassword);
        checkBoxRecordar = viewLayout.findViewById(R.id.checkBoxRecordar);
    }

    private void events() {
        btnLogin.setOnClickListener(this);
        textViewRegister.setOnClickListener(this);
    }

    private void retrofitInit(){
        miniTwitterClient = MiniTwitterClient.getInstance();
        miniTwitterService = miniTwitterClient.getMiniTwitterService();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.buttonLogin:

                toLogin();
                break;
            case R.id.textViewRegister:
                toRegister();
                break;
        }
    }

    private void toRegister() {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragmentLayout, new RegistrerFragment())
                .commit();
    }

    private void toLogin() {
        String email = editTextEmail.getText().toString();
        String password = editTextPassword.getText().toString();


        if(validation()){
            btnLogin.setEnabled(false);
            progressBarLogin.setVisibility(View.VISIBLE);

            Call<ResponseAuth> call = miniTwitterService.doLogin(new RequestLogin(email, password));
            call.enqueue(new Callback<ResponseAuth>() {
                @Override
                public void onResponse(Call<ResponseAuth> call, Response<ResponseAuth> response) {
                    btnLogin.setEnabled(true);
                    progressBarLogin.setVisibility(View.GONE);

                    if(response.isSuccessful()){
                        ResponseAuth respuesta = response.body();

                        SharedPreferencesManager.setStringValue(Constantes.PREF_TOKEN, respuesta.getToken());
                        SharedPreferencesManager.setStringValue(Constantes.PREF_USUARIO, respuesta.getUsername());
                        SharedPreferencesManager.setStringValue(Constantes.PREF_EMAIL, respuesta.getEmail());
                        SharedPreferencesManager.setStringValue(Constantes.PREF_PHOTO, respuesta.getPhotoUrl());
                        SharedPreferencesManager.setStringValue(Constantes.PREF_FEC_CREATE, respuesta.getCreated());
                        SharedPreferencesManager.setBooleanValue(Constantes.PREF_IS_RECORDAR, checkBoxRecordar.isChecked());
                        Toast.makeText(getContext(), "Login exitoso "+respuesta.getUsername(), Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(getActivity(), DashboardActivity.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                    }else {
                        Toast.makeText(getContext(), "Login incorrecto, valide su informacion", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseAuth> call, Throwable t) {
                    btnLogin.setEnabled(true);
                    progressBarLogin.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "No hay conexion a internet", Toast.LENGTH_LONG).show();
                }
            });


        }



    }
    private boolean validation(){
        boolean estado = true;

        if(editTextEmail.getText().toString().equals("")){

            editTextEmail.setError("Este campo es requerido");
            estado = false;
        }else if (editTextPassword.getText().toString().equals("")){
            editTextPassword.setError("Este campo es requerido");
            estado = false;
        }

        return estado;
    }
}