package co.com.ngox.minitwitter.retrofit;

import java.io.IOException;

import co.com.ngox.minitwitter.common.Constantes;
import co.com.ngox.minitwitter.common.SharedPreferencesManager;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        String token = SharedPreferencesManager.getStringValue(Constantes.PREF_TOKEN);
        Request request = chain.request() // obtiene el request
                .newBuilder() // inicia la construccion
                .addHeader("Authorization", "Bearer "+token) // añade al encabezado
                .build(); // construye un nuevo request

        return chain.proceed(request); // procesa el request

    }
}
