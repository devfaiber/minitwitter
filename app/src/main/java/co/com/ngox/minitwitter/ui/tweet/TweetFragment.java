package co.com.ngox.minitwitter.ui.tweet;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;


import co.com.ngox.minitwitter.R;
import co.com.ngox.minitwitter.data.TweetViewModel;
import co.com.ngox.minitwitter.common.Constantes;
import co.com.ngox.minitwitter.retrofit.AuthTwitterClient;
import co.com.ngox.minitwitter.retrofit.response.Tweet;
import co.com.ngox.minitwitter.retrofit.services.AuthTwitterService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 */
public class TweetFragment extends Fragment {

    private int tweetListType = 1;

    // TODO: Customize parameters
    private int mColumnCount = 1;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private MyTweetRecyclerViewAdapter adapter;
    private List<Tweet> tweetList;

    private AuthTwitterService authTwitterService;
    private AuthTwitterClient authTwitterClient;
    private TweetViewModel tweetViewModel;


    public static TweetFragment newInstance(int tweetListType){
        TweetFragment tweetFragment = new TweetFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constantes.TWEET_LIST_TYPE, tweetListType);
        tweetFragment.setArguments(bundle);
        return tweetFragment;
    }


    public TweetFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tweetViewModel = new ViewModelProvider(getActivity()).get(TweetViewModel.class);

        if(getArguments() != null){
            tweetListType = getArguments().getInt(Constantes.TWEET_LIST_TYPE);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tweet_list, container, false);

        // Set the adapter

        Context context = view.getContext();
        recyclerView = view.findViewById(R.id.list);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefresh);

        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimaryDark));

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true); // actualizando..

                if(tweetListType == Constantes.TWEET_LIST_ALL){
                    loadRefreshData();
                } else if (tweetListType == Constantes.TWEET_LIST_FAV){
                    loadRefreshFav();
                }

            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(context));

        //retrofitInit();
        //loadDataTweet();

        adapter = new MyTweetRecyclerViewAdapter(
                getActivity(),
                tweetList
        );
        recyclerView.setAdapter(adapter);


        if(tweetListType == Constantes.TWEET_LIST_ALL){
            loadData();
        } else if(tweetListType == Constantes.TWEET_LIST_FAV) {
            loadFav();
        }



        return view;
    }

    private void loadFav() {
        tweetViewModel.getAllFavTweets().observe(getActivity(), new Observer<List<Tweet>>() {
            @Override
            public void onChanged(List<Tweet> tweets) {
                adapter.setData(tweets);
            }
        });
    }
    private void loadRefreshFav(){
        tweetViewModel.getRefreshFavTweets().observe(getActivity(), new Observer<List<Tweet>>() {
            @Override
            public void onChanged(List<Tweet> tweets) {
                adapter.setData(tweets);
                swipeRefreshLayout.setRefreshing(false);
                tweetViewModel.getAllFavTweets().removeObserver(this);
            }
        });
    }

    private void loadData(){

        tweetViewModel.getAllTweets().observe(getActivity(), new Observer<List<Tweet>>() {
            @Override
            public void onChanged(List<Tweet> tweets) {
                adapter.setData(tweets);
            }
        });

    }
    private void loadRefreshData(){

        tweetViewModel.getRefreshTweets().observe(getActivity(), new Observer<List<Tweet>>() {
            @Override
            public void onChanged(List<Tweet> tweets) {

                adapter.setData(tweets);

                swipeRefreshLayout.setRefreshing(false); // ya se actualizo deja de actualizar

                // desactivo el observe una vez ya haya actualizado para que no se quede escuchando eventos
                tweetViewModel.getRefreshTweets().removeObserver(this);
            }

        });

    }

    // carga los tweet
    private void loadDataTweet(){

        Call<List<Tweet>> call = authTwitterService.getAllTweets();

        call.enqueue(new Callback<List<Tweet>>() {
            @Override
            public void onResponse(Call<List<Tweet>> call, Response<List<Tweet>> response) {
                if(response.isSuccessful()){
                    tweetList = response.body();
                    // recibe 3 parametros 1 contexto 2 lista y 3 viewmodel
                    adapter = new MyTweetRecyclerViewAdapter(
                            getActivity(),
                            tweetList
                    );
                    recyclerView.setAdapter(adapter);
                }else{
                    Toast.makeText(getActivity(), "Error datos no encontrados", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<List<Tweet>> call, Throwable t) {
                Toast.makeText(getActivity(), "Error de wifi", Toast.LENGTH_LONG).show();
            }
        });



    }

    private void retrofitInit(){
        authTwitterClient = AuthTwitterClient.getInstance();
        authTwitterService = authTwitterClient.getAuthTwitterService();
    }
}