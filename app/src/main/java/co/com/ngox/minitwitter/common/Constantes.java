package co.com.ngox.minitwitter.common;

public class Constantes {
    public final static String CODE_API = "UDEMYANDROID";
    public final static String BASE_API_URL = "https://minitwitter.com:3001/apiv1/";
    public final static String UPLOAD_PHOTOS_API_URL = "https://minitwitter.com/apiv1/uploads/photos/";

    // nombre del sharedpreferences que guarda el token
    public final static String SHARED_PREFERENCES_1 = "co.com.ngox.minitwitter.tokenSharedPreferences";

    // nombres de sharedPreferences
    public final static String PREF_TOKEN = "user_token";
    public final static String PREF_USUARIO = "user_usuario";
    public final static String PREF_EMAIL = "user_email";
    public final static String PREF_PHOTO = "user_photo";
    public final static String PREF_ACTIVE = "user_active";
    public final static String PREF_FEC_CREATE = "user_fec_create";
    public final static String PREF_IS_RECORDAR = "user_recordar";



    // argumentos
    public final static String TWEET_LIST_TYPE = "TWEET_LIST_TYPE";
    public final static int TWEET_LIST_ALL = 1;
    public final static int TWEET_LIST_FAV = 2;
    public final static String ARG_TWEET_ID = "idTweet";

}
