package co.com.ngox.minitwitter.ui.tweet;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.navigation.NavigationView;

import co.com.ngox.minitwitter.R;
import co.com.ngox.minitwitter.common.Constantes;
import co.com.ngox.minitwitter.data.TweetViewModel;

public class BottomModalTweetFragment extends BottomSheetDialogFragment {

    private TweetViewModel mViewModel;
    private View view;
    private int idTweet;

    public static BottomModalTweetFragment newInstance(int tweet_id) {
        BottomModalTweetFragment bottomModalTweetFragment = new BottomModalTweetFragment();
        Bundle args = new Bundle();
        args.putInt(Constantes.ARG_TWEET_ID, tweet_id);
        bottomModalTweetFragment.setArguments(args);
        return bottomModalTweetFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            idTweet = getArguments().getInt(Constantes.ARG_TWEET_ID);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {




        view  = inflater.inflate(R.layout.botton_modal_tweet_fragment, container, false);

        final NavigationView nav = view.findViewById(R.id.navigation_view_bottom);
        nav.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                switch (id){
                    case R.id.delete_tweet:
                        mViewModel.deleteTweet(idTweet);
                        getDialog().dismiss();
                        break;
                }

                return false;
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(getActivity()).get(TweetViewModel.class);
        // TODO: Use the ViewModel
    }

}