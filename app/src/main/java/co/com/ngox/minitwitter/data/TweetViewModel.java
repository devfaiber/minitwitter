package co.com.ngox.minitwitter.data;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import co.com.ngox.minitwitter.ui.tweet.BottomModalTweetFragment;
import co.com.ngox.minitwitter.retrofit.response.Tweet;

public class TweetViewModel extends AndroidViewModel {

    private TweetRepository tweetRepository;
    private LiveData<List<Tweet>> allTweets;
    private LiveData<List<Tweet>> allFavTweets;

    public TweetViewModel(@NonNull Application application) {
        super(application);

        tweetRepository = new TweetRepository(application);
        allTweets = tweetRepository.getAllTweets();
    }

    public LiveData<List<Tweet>> getAllTweets(){
        return allTweets;
    }
    public LiveData<List<Tweet>> getRefreshTweets(){
        allTweets = tweetRepository.getAllTweets();
        return allTweets;
    }

    public void insetTweet(String mensaje){
        tweetRepository.createTweet(mensaje);
    }

    public void likeTweet(int idTweet){
        tweetRepository.likeTweet(idTweet);
    }

    public LiveData<List<Tweet>> getAllFavTweets(){
        allFavTweets = tweetRepository.getAllFavTweets();

        return allFavTweets;
    }

    public LiveData<List<Tweet>> getRefreshFavTweets(){
        getAllTweets();

        return allFavTweets;
    }

    public void deleteTweet(int idTweet){
        tweetRepository.deleteTweet(idTweet);
    }


    public void openDialogTweet(Context context, int idTweet){
        BottomModalTweetFragment dialogo = BottomModalTweetFragment.newInstance(idTweet);
        dialogo.show(((AppCompatActivity)context).getSupportFragmentManager(), "B");
    }

}
