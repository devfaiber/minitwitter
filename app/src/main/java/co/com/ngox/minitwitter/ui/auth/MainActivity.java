package co.com.ngox.minitwitter.ui.auth;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import co.com.ngox.minitwitter.R;
import co.com.ngox.minitwitter.ui.auth.LoginFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentLayout, new LoginFragment())
                .commit();

    }

}