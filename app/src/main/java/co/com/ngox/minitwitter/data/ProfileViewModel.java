package co.com.ngox.minitwitter.data;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import co.com.ngox.minitwitter.retrofit.request.RequestPutProfile;
import co.com.ngox.minitwitter.retrofit.response.ResponseProfile;

public class ProfileViewModel extends AndroidViewModel {

    private ProfileRepository profileRepository;
    private LiveData<ResponseProfile> userProfile;

    public ProfileViewModel(@NonNull Application application) {
        super(application);
        profileRepository = new ProfileRepository(application);
        userProfile = profileRepository.getUserProfile();
    }

    public LiveData<ResponseProfile> getProfile(){
        return userProfile;
    }

    public void putProfile(RequestPutProfile requestPutProfile){
        profileRepository.putUserProfile(requestPutProfile);
    }


}